def reverser(&prc)
  prc.call.split.map(&:reverse).join(" ")
end

def adder(add = 1, &prc)
  prc.call + add
end

def repeater(rep_times = 1, &prc)
  rep_times.times { prc.call }
end
